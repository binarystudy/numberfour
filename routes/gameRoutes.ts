import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config.js';
import { texts } from '../data.js';

const router = Router();

router.get('/', (req, res) => {
  const page = path.join(HTML_FILES_PATH, 'game.html');
  res.sendFile(page);
});

router.get('/texts/:id', (req, res, next) => {
  const { id } = req.params;

  if (!id || !/^\d+$/.test(id) || !texts[Number(id)]) {
    next();
  }
  res.json(texts[Number(id)]);
});

export default router;
