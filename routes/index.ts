import { Express } from 'express';
import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';

export default (app: Express) => {
  app.use('/login', loginRoutes);
  app.use('/game', gameRoutes);
};
