import { Socket, Server } from 'socket.io';
import { getUser } from './users/getUser';
import {
  ChangeStatus,
  CreateRoom,
  JoinRoom,
  LeaveRoom,
  Disconnecting,
  UpdateProgress,
} from './rooms';
import { ComentatorSocket, ComentatorIo } from './comentator';
import { rooms } from './data';
import { SOCKET_EVENT } from './config';
import { CourseOfTheGame, processGame } from './game';
import { message } from '../data';
import { roomsForComment } from './comentator';

export default (io: Server) => {
	io.sockets
  const commentatorIo = new ComentatorIo({ socket: io, message, roomsForComment });
  const courseOfGame = new CourseOfTheGame(commentatorIo, processGame);
  courseOfGame.timerOn();
  io.on('connection', (socket: Socket) => {
    const userName = getUser(socket);
    const commentator = new ComentatorSocket({ socket, message, roomsForComment });

    if (!userName) {
      socket.emit(SOCKET_EVENT.connectionUser, 'error');
      return;
    }
    socket.emit(SOCKET_EVENT.connectionUser, 'success');
    socket.emit(SOCKET_EVENT.updateRooms, rooms.mapRooms());

    new Disconnecting({
      socket: commentator,
      userName,
      courseOfGame,
      event: SOCKET_EVENT.disconnecting,
    });
    new UpdateProgress({
      socket: commentator,
      userName,
      courseOfGame,
      event: SOCKET_EVENT.updateProgress,
    });
    new LeaveRoom({ socket: commentator, userName, courseOfGame, event: SOCKET_EVENT.leaveRoom });
    new ChangeStatus({
      socket: commentator,
      userName,
      courseOfGame,
      event: SOCKET_EVENT.changeStatus,
    });
    new JoinRoom({ socket: commentator, userName, courseOfGame, event: SOCKET_EVENT.joinRoom });
    new CreateRoom({ socket: commentator, userName, courseOfGame, event: SOCKET_EVENT.createRoom });
  });
};
