import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from '../config';

export interface IRoom {
  secondsBeforeStart: number;
  secondsForGame: number;
}

export class RoomsWithGame {
  rooms: { before: Map<string, IRoom>; game: Map<string, IRoom>; newGame: Map<string, IRoom> } = {
    before: new Map(),
    game: new Map(),
    newGame: new Map(),
  };

  removeRoom(roomName: string): void {
    const filter = ([name, _]: [string, IRoom]) => roomName !== name;
    this.rooms = {
      game: new Map(this.getGame().filter(filter)),
      newGame: new Map(this.getNewGame().filter(filter)),
      before: new Map(this.getBefore().filter(filter)),
    };
  }

  addRoom(roomName: string): void {
    this.rooms = {
      ...this.rooms,
      before: this.rooms.before.set(roomName, {
        secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME,
        secondsForGame: SECONDS_FOR_GAME,
      }),
    };
  }

  changeTime(): void {
    const before = this.getBefore();
    const newGame = before.filter(([_, value]) => value.secondsBeforeStart === 0);
    const newBefore = before
      .filter(([_, value]) => value.secondsBeforeStart !== 0)
      .map(
        ([key, value]) =>
          [key, { ...value, secondsBeforeStart: --value.secondsBeforeStart }] as [string, IRoom]
      );
    const game = [...newGame, ...this.getGame()]
      .filter(([_, value]) => value.secondsForGame !== 0)
      .map(
        ([key, value]) =>
          [key, { ...value, secondsForGame: --value.secondsForGame }] as [string, IRoom]
      );
    this.rooms = {
      before: new Map(newBefore),
      newGame: new Map(newGame),
      game: new Map(game),
    };
  }

  getBefore(): Array<[string, IRoom]> {
    return Array.from(this.rooms.before);
  }

  getGame(): Array<[string, IRoom]> {
    return Array.from(this.rooms.game);
  }

  getNewGame(): Array<[string, IRoom]> {
    return Array.from(this.rooms.newGame);
  }
}

export const roomsWithGame = new RoomsWithGame();

export type IRoomsWithGame = InstanceType<typeof RoomsWithGame>;
