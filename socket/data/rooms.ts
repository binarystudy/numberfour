import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';
import { users } from './users';

export interface IUser {
  ready: boolean;
  endGame: boolean;
  progress: number;
  numberLitters: number;
}

export interface IRoomValue {
  users: Map<string, IUser>;
  gameStart: boolean;
  textLength: number;
  leaders: Array<[string, number]>[];
}

interface IUserMap extends IUser {
  numberGames: number;
}

export type MapUsersReturn = {
  users: Array<[string, IUserMap]>;
  roomName: string;
  gameStart: boolean;
  textLength: number;
};

class Rooms {
  rooms: Map<string, IRoomValue> = new Map();
  users = users;

  mapUsers(roomName: string): MapUsersReturn | {} {
    const room = this.rooms.get(roomName);
    if (!room) {
      return {};
    }
    const users: MapUsersReturn['users'] = Array.from(room.users).map(([name, value]) => [
      name,
      { ...value, numberGames: this.users.get(name) || 0 },
    ]);
    return { users, roomName, gameStart: room.gameStart, textLength: room.textLength };
  }

  mapRooms(): Array<[string, number]> {
    return Array.from(this.rooms)
      .filter(([_, value]) => !value.gameStart && value.users.size < MAXIMUM_USERS_FOR_ONE_ROOM)
      .map(([key, value]) => [key, value.users.size]);
  }

  addRoom(roomName: string, userName: string) {
    this.rooms.set(roomName, {
      gameStart: false,
      users: new Map(),
      textLength: 0,
      leaders: [],
    });
    this.addUser(roomName, userName);
  }

  addUser(roomName: string, userName: string): void {
    const room = this.rooms.get(roomName);
    if (!room) {
      return;
    }
    room.users.set(userName, this.defUser());
  }

  defUser(): IUser {
    return {
      progress: 0,
      endGame: false,
      ready: false,
      numberLitters: 0,
    };
  }

  delUser(roomName: string, userName: string): void {
    const room = this.rooms.get(roomName);
    if (!room) {
      return;
    }
    room.users.delete(userName);
  }

  delRoom(roomName: string): void {
    const room = this.rooms.get(roomName);
    if (room && !room.users.size) {
      this.rooms.delete(roomName);
    }
  }

  hasRoom(roomName: string): boolean {
    return this.rooms.has(roomName);
  }

  roomFull(roomName: string): boolean {
    const room = this.rooms.get(roomName);
    if (!room) {
      return true;
    }
    return room.users.size >= MAXIMUM_USERS_FOR_ONE_ROOM;
  }

  getRoom(roomName: string | undefined) {
    if (!roomName) {
      return;
    }
    const room = this.rooms.get(roomName);
    return room && Object.assign({}, room);
  }

  setRoom(roomName: string, data: IRoomValue): void {
    this.rooms.set(roomName, data);
  }

  resetRoom(roomName: string): void {
    const room = this.rooms.get(roomName);
    if (!room) {
      return;
    }
    const users = Array.from(room.users).map(
      ([name, _]) => [name, this.defUser()] as [string, IUser]
    );
    this.rooms.set(roomName, {
      gameStart: false,
      users: new Map(users),
      textLength: 0,
      leaders: [],
    });
  }

  endedGame(roomName: string): void {
    Array.from(this.getRoom(roomName)?.users || []).forEach(([userName, _]) => {
      const userCountGame = this.users.get(userName) || 0;
      this.users.set(userName, userCountGame + 1);
    });
  }
}

const rooms = new Rooms();
type IRooms = InstanceType<typeof Rooms>;

export { rooms, IRooms };
