import { Rooms } from './rooms';
import { SOCKET_EVENT } from '../config';
import { getCurrentRoom } from '../helpers';

export class LeaveRoom extends Rooms {
  declare roomName;
  listener() {
    this.roomName = getCurrentRoom(this.socket);
    if (!this.roomName.length) {
      return;
    }
    this.userLeavRoom();
    this.checkStatusRoom();
    this.socket.emit(SOCKET_EVENT.leavedRoom, this.rooms.mapRooms());
  }
}
