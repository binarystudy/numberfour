import { Rooms } from './rooms';
import { SOCKET_EVENT } from '../config';
import { getCurrentRoom } from '../helpers';

export class JoinRoom extends Rooms {
  declare roomName;
  listener({ roomName }: { roomName: string }) {
    if (!roomName) {
      return;
    }
    this.roomName = roomName;
    const prevRoomName = getCurrentRoom(this.socket);
    const room = this.rooms.hasRoom(this.roomName);

    const isPrevRoom = this.roomName === prevRoomName;
    const roomFull = this.rooms.roomFull(this.roomName);
    if (!room || !this.userName || isPrevRoom || roomFull) {
      return;
    }

    this.userLeavRoom();

    this.rooms.addUser(this.roomName, this.userName);
    this.socket.join(this.roomName);
    this.socket.to(this.roomName).emit(SOCKET_EVENT.updateRoom, this.rooms.mapUsers(this.roomName));
    this.socket.emit(SOCKET_EVENT.joinRoomDone, this.rooms.mapUsers(this.roomName));
    this.socket.broadcast.emit(SOCKET_EVENT.updateRooms, this.rooms.mapRooms());
  }
}
