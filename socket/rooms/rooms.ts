import { IComentatorSocket } from '../comentator';
import { rooms, roomsWithGame } from '../data';
import { ICourseOfTheGame } from '../game';
import { SOCKET_EVENT } from '../config';
import { randomText } from '../../helpers';
import { texts } from '../../data';

interface Props {
  socket: IComentatorSocket;
  userName: string;
  event: string;
  courseOfGame: ICourseOfTheGame;
}

export abstract class Rooms {
  socket: IComentatorSocket;
  courseOfGame: ICourseOfTheGame;
  userName: string;
  rooms = rooms;
  event: string;
  roomsWithGame = roomsWithGame;
  listenerBind: (props: Record<string, string>) => void;
  abstract roomName: string;
  constructor({ socket, userName, courseOfGame, event }: Props) {
    this.socket = socket;
    this.userName = userName;
    this.courseOfGame = courseOfGame;
    this.event = event;
    this.listenerBind = this.listener.bind(this);
    this.socket.on(this.event, this.listenerBind);
  }

  listener(props: Record<string, string>): void {}

  userLeavRoom(): void {
    if (!this.userName || !this.roomName) {
      return;
    }
    this.rooms.delUser(this.roomName, this.userName);
    this.rooms.delRoom(this.roomName);

    this.socket.leave(this.roomName);

    if (this.rooms.hasRoom(this.roomName)) {
      this.socket
        .to(this.roomName)
        .emit(SOCKET_EVENT.updateRoom, this.rooms.mapUsers(this.roomName));
    } else {
      this.roomsWithGame.removeRoom(this.roomName);
    }

    this.socket.broadcast.emit(SOCKET_EVENT.updateRooms, this.rooms.mapRooms());
  }

  checkStatusRoom(): void {
    const room = this.rooms.getRoom(this.roomName);
    if (!room || room.gameStart) {
      return;
    }
    const ready = Array.from(room.users).filter(([_, value]) => value.ready);
    if (ready.length !== room.users.size) {
      return;
    }
    this.roomsWithGame.addRoom(this.roomName);
    const idText = randomText();
    const response = { idText };

    this.rooms.setRoom(this.roomName, {
      ...room,
      gameStart: true,
      textLength: texts[idText].length,
    });

    this.socket.emit(SOCKET_EVENT.startGame, response);
    this.socket.to(this.roomName).emit(SOCKET_EVENT.startGame, response);
    this.socket.broadcast.emit(SOCKET_EVENT.updateRooms, this.rooms.mapRooms());
  }
}
