import { Rooms } from './rooms';
import { SOCKET_EVENT } from '../config';
import { getCurrentRoom } from '../helpers';

export class ChangeStatus extends Rooms {
  declare roomName;
  listener() {
    this.roomName = getCurrentRoom(this.socket);
    const room = this.rooms.getRoom(this.roomName);
    if (!room || !this.roomName.length) {
      return;
    }
    const user = room.users.get(this.userName);
    if (!user) {
      return;
    }
    room.users.set(this.userName, {
      ...user,
      ready: !user.ready,
    });
    this.rooms.setRoom(this.roomName, room);
    this.socket.emit(SOCKET_EVENT.updateRoom, this.rooms.mapUsers(this.roomName));
    this.socket.to(this.roomName).emit(SOCKET_EVENT.updateRoom, this.rooms.mapUsers(this.roomName));
    this.checkStatusRoom();
  }
}
