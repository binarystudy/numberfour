import { Rooms } from './rooms';
import { SOCKET_EVENT } from '../config';
import { getCurrentRoom } from '../helpers';

export class UpdateProgress extends Rooms {
  declare roomName;
  listener({ numberLittersString }: { numberLittersString: string }) {
    this.roomName = getCurrentRoom(this.socket);
    const room = this.rooms.getRoom(this.roomName);
    if (!room || !this.roomName.length) {
      return;
    }
    const user = room.users.get(this.userName);
    if (!user) {
      return;
    }
    const numberLitters = Number(numberLittersString);
    room.users.set(this.userName, {
      ...user,
      ready: user.ready,
      endGame: numberLitters === room.textLength,
      progress: +((numberLitters * 100) / room.textLength).toFixed(1),
      numberLitters,
    });
    this.rooms.setRoom(this.roomName, room);
    this.socket.emit(SOCKET_EVENT.updatedProgress, this.rooms.mapUsers(this.roomName));
    this.socket
      .to(this.roomName)
      .emit(SOCKET_EVENT.updatedProgress, this.rooms.mapUsers(this.roomName));
  }
}
