import { Rooms } from './rooms';
import { SOCKET_EVENT } from '../config';

export class CreateRoom extends Rooms {
  declare roomName;
  listener({ roomName }: { roomName: string }) {
    if (!roomName) {
      return;
    }
    this.roomName = roomName;
    if (this.rooms.hasRoom(this.roomName)) {
      this.socket.emit('CREATED_ROOM', 'error');
    } else {
      this.rooms.addRoom(this.roomName, this.userName);
      this.socket.join(this.roomName);
      this.socket.broadcast.emit(SOCKET_EVENT.updateRooms, this.rooms.mapRooms());
      this.socket.emit(SOCKET_EVENT.createdRoom, this.rooms.mapUsers(this.roomName));
    }
  }
}
