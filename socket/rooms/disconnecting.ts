import { Rooms } from './rooms';
import { users } from '../data';
import { getCurrentRoom } from '../helpers';

export class Disconnecting extends Rooms {
  declare roomName;
  listener() {
    this.roomName = getCurrentRoom(this.socket);
    users.delete(this.userName);
    if (!this.roomName.length) {
      return;
    }
    this.userLeavRoom();
    this.checkStatusRoom();
  }
}
