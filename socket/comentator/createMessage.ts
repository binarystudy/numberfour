import { IUser, MapUsersReturn } from '../data';
import { MAX_LENGTH_LEADERS, MIN_LETTERS_FOR_FINISH_COMMENT } from '../config';

type MessageContent = Record<string, string | ((message: string) => string)>;

class CreateMessage {
  message: MessageContent;
  constructor(message: MessageContent) {
    this.message = message;
  }

  updateRoom(response: MapUsersReturn): string {
    const users = response.users.map(
      ([userName, { numberGames }], index) => `${index + 1}) ${userName} - ${numberGames} ігр(и)!`
    );
    return `Зараз в кімнаті - ${response.roomName}, присутні користувачи: ${users.join('; ')}`;
  }

  createdRoom(): string {
    return this.message.createRoomGreeting as string;
  }

  joinRoomDone(): string {
    return this.message.joinRoomDoneGreeting as string;
  }

  timerBeforeStart(message: string): string {
    return `${message}! Гра почнеться через декілька секунд, нехай щастить!`;
  }

  commentEverFewSeconds(leaders: string[], seconds: number): string {
    const leadersString = leaders.map((name, i) => `${i + 1} місце - ${name}`);
    return `Поточный стан: ${leadersString.join(', ')}. До завершення - ${seconds} секунд(и)!`;
  }

  updateProgress(users: MapUsersReturn['users'], textLength: number): string | undefined {
    const data = users
      .map(([name, { numberLitters }]) => [name, textLength - numberLitters] as [string, number])
      .sort((a, b) => a[1] - b[1]);
    if (data[0][1] !== MIN_LETTERS_FOR_FINISH_COMMENT) {
      return;
    }
    const secondGamer = data[1];
    const secondText = secondGamer ? `, а на другому місці: ${secondGamer[0]}` : '';
    return `На першому місці користувач: ${data[0][0]}${secondText}`;
  }

  endedGame(leaders: [string, number][][]): string | undefined {
    const message = leaders
      .slice(0, MAX_LENGTH_LEADERS)
      .map((leadersRang, index) => {
        const leadersMap = leadersRang.map(([name, seconds]) => `${name} за ${seconds} секунд(у)`);
        return `${index + 1}) ${leadersMap.join(', ')}`;
      })
      .join('; ');
    return message.length ? `Game Results! ${message}` : undefined;
  }
}

export { CreateMessage };
