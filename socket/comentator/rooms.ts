interface IRoom {
  startGameComment: boolean;
  secondsForCommentator: number;
  message: string;
}

export class RoomsForCommentator {
  rooms: Map<string, IRoom> = new Map();

  set(roomName: string, value: IRoom) {
    this.rooms.set(roomName, value);
  }

  get(roomName: string | undefined) {
    return roomName ? this.rooms.get(roomName) : undefined;
  }

  has(roomName: string) {
    return this.rooms.has(roomName);
  }

  resetData() {
    return {
      startGameComment: false,
      message: '',
      secondsForCommentator: 0,
    };
  }

  remove(roomName: string): void {
    this.rooms.delete(roomName);
  }

  add(roomName: string | undefined): void {
    if (!roomName) {
      return;
    }
    this.rooms.set(roomName, this.resetData());
  }
}

export const roomsForComment = new RoomsForCommentator();
