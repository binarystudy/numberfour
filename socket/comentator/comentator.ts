import { Server, Socket, BroadcastOperator, Namespace } from 'socket.io';
import { SECONDS_FOR_COMMENTATOR, SOCKET_EVENT } from '../config';
import { CreateMessage } from './createMessage';
import { RoomsForCommentator } from './rooms';
import { rooms } from '../data';
import { sortUserByProgress } from '../helpers';
import { EventsMap } from 'socket.io/dist/typed-events';

abstract class AbstractComent {
  abstract socket: Server | Socket;
  abstract emit(event: string, ...args: any[]): void;
  abstract switchEvent(event: string, ...args: any[]): string | undefined;
  roomName: string | undefined;
  isTo: boolean = false;
  createMessage: InstanceType<typeof CreateMessage>;
  roomsForComment: InstanceType<typeof RoomsForCommentator>;
  activeRooms = rooms;
  constructor(
    message: Record<string, string>,
    roomsForComment: InstanceType<typeof RoomsForCommentator>
  ) {
    this.createMessage = new CreateMessage(message);
    this.roomsForComment = roomsForComment;
  }

  on(event: string, listener: (...args: any[]) => void): void {
    this.socket.on(event, listener);
  }

  to(roomName: string) {
    this.roomName = roomName;
    this.isTo = true;
    return this;
  }

  emitSuper(
    socket: Server | Socket | BroadcastOperator<EventsMap> | Namespace,
    event: string,
    ...args: any[]
  ): void {
    socket.emit(event, ...args);
    const message = this.switchEvent(event, ...args);
    if (!message || !Boolean(message.length)) {
      return;
    }
    socket.emit(SOCKET_EVENT.comment, message);
    this.roomName = undefined;
    this.isTo = false;
  }
}

interface Commentators {
  message: Record<string, string>;
  roomsForComment: InstanceType<typeof RoomsForCommentator>;
}

interface CommnetatorsSocket extends Commentators {
  socket: Socket;
}

interface CommnetatorsIo extends Commentators {
  socket: Server;
}

class ComentatorSocket extends AbstractComent {
  socket: Socket;
  isBroadCast: boolean = false;
  constructor({ message, socket, roomsForComment }: CommnetatorsSocket) {
    super(message, roomsForComment);
    this.socket = socket;
  }

  leave(roomName: string) {
    this.socket.leave(roomName);
  }

  join(roomName: string) {
    this.socket.join(roomName);
  }

  get rooms(): Set<string> {
    return this.socket.rooms;
  }

  get broadcast() {
    this.isBroadCast = true;
    return this;
  }

  emit(event: string, ...args: any[]): void {
    let socket;
    if (this.isBroadCast) {
      socket = this.socket.broadcast;
    } else {
      socket = this.isTo && this.roomName ? this.socket.to(this.roomName) : this.socket;
    }
    this.roomName = undefined;
    this.emitSuper(socket, event, ...args);
    this.isBroadCast = false;
  }

  switchEvent(event: string, ...args: any[]): string | undefined {
    const response = args[0];
    const { gameStart, roomName, users, textLength } = response;
    const room = this.roomsForComment.get(roomName);

    switch (event) {
      case SOCKET_EVENT.createdRoom:
        this.roomsForComment.add(roomName);
        return this.createMessage.createdRoom();
      case SOCKET_EVENT.joinRoomDone:
        return this.createMessage.joinRoomDone();
      case SOCKET_EVENT.updateRoom:
        const updateRoom = this.createMessage.updateRoom(response);
        if (!gameStart && room) {
          this.roomsForComment.set(roomName, { ...room, message: updateRoom });
        }
        return updateRoom;
      case SOCKET_EVENT.updatedProgress:
        return this.createMessage.updateProgress(users, textLength);
      default:
        return;
    }
  }
}

class ComentatorIo extends AbstractComent {
  socket: Server;
  isSockets: boolean = false;
  constructor({ message, socket, roomsForComment }: CommnetatorsIo) {
    super(message, roomsForComment);
    this.socket = socket;
  }

  get sockets() {
    this.isSockets = true;
    return this;
  }

  emit(event: string, ...args: any[]): void {
    let socket;
    if (this.isSockets) {
      socket = this.socket.sockets;
    } else {
      socket = this.roomName ? this.socket.to(this.roomName) : this.socket;
    }
    this.emitSuper(socket, event, ...args);
    this.isSockets = false;
  }

  switchEvent(event: string, ...args: any[]): string | undefined {
    const room = this.roomsForComment.get(this.roomName);
    const activeRoom = this.activeRooms.getRoom(this.roomName);
    if (!this.roomName || !room) {
      return;
    }

    switch (event) {
      case SOCKET_EVENT.changTimerBeforeStart:
        if (room.startGameComment) {
          return;
        }
        this.roomsForComment.set(this.roomName, {
          ...room,
          startGameComment: true,
        });
        return this.createMessage.timerBeforeStart(room.message);
      case SOCKET_EVENT.changTimerForGame:
        const seconds = room.secondsForCommentator;
        const isComment = seconds === SECONDS_FOR_COMMENTATOR;
        this.roomsForComment.set(this.roomName, {
          ...room,
          secondsForCommentator: !isComment ? seconds + 1 : 0,
        });
        if (!isComment || !activeRoom) {
          return;
        }
        const leaders = Array.from(activeRoom.users)
          .sort(sortUserByProgress)
          .map(([name, _]) => name);
        return this.createMessage.commentEverFewSeconds(leaders, args[0]);
      case SOCKET_EVENT.endedGame:
        this.roomsForComment.set(this.roomName, {
          ...this.roomsForComment.resetData(),
        });
        return this.createMessage.endedGame(args[0]);
      default:
        return;
    }
  }
}

type IComentatorSocket = InstanceType<typeof ComentatorSocket>;

type IComentatorIo = InstanceType<typeof ComentatorIo>;

export { ComentatorSocket, ComentatorIo, IComentatorIo, IComentatorSocket };
