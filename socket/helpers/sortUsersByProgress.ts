import { IUser } from '../data';

export const sortUserByProgress = (a: [string, IUser], b: [string, IUser]) =>
  b[1].progress - a[1].progress;
