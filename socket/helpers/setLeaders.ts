import { SECONDS_FOR_GAME } from '../config';
import { IUser } from '../data';
import { sortUserByProgress } from './sortUsersByProgress';

export const lineLeadersName = (leaders: [string, number][][]) => {
  return leaders.reduce((prev, leader) => [...prev, ...leader], []).map(([name, _]) => name);
};

export const setLeaders = (users: [string, IUser][], leaders: [string, number][][]) => {
  const lineLeaders = lineLeadersName(leaders);
  return users
    .filter((user) => !lineLeaders.includes(user[0]))
    .sort(sortUserByProgress)
    .reduce((prev, user) => {
      const prevLiders = prev[prev.length - 1];
      const modifyUser: [string, number] = [user[0], SECONDS_FOR_GAME];
      if (!prevLiders) {
        return [[modifyUser]];
      }
      // if (prevLiders && prevLiders[0][1].progress === user[1].progress) {
      //   return [[...prevLiders, modifyUser]];
      // }
      return [prevLiders, [modifyUser]];
    }, [] as [string, number][][]);
};
