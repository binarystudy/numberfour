import { Socket } from 'socket.io';
import { IComentatorSocket } from '../comentator';
import { rooms } from '../data';

export const getCurrentRoom = (socket: IComentatorSocket): string => {
  return Array.from(socket.rooms.keys()).find((roomName) => rooms.hasRoom(roomName)) || '';
};
