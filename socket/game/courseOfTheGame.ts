import { GAME_INTERVAL } from '../config';
import { IComentatorIo } from '../comentator';

class CourseOfTheGame {
  io: IComentatorIo;
  gameInterval: NodeJS.Timeout | null = null;
  process: (io: IComentatorIo) => void;
  constructor(io: IComentatorIo, process: (io: IComentatorIo) => void) {
    this.io = io;
    this.process = process;
  }

  timerOff(): void {
    if (this.gameInterval) {
      clearInterval(this.gameInterval);
      this.gameInterval = null;
    }
  }

  timerOn(): void {
    this.gameInterval = setInterval(() => {
      this.process(this.io);
    }, GAME_INTERVAL);
  }
}

type ICourseOfTheGame = InstanceType<typeof CourseOfTheGame>;

export { CourseOfTheGame, ICourseOfTheGame };
