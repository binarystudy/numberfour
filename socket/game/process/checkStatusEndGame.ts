import { setLeaders } from '../../helpers';
import { SOCKET_EVENT } from '../../config';
import { IRooms, IRoomsWithGame } from '../../data';
import { IComentatorIo } from '../../comentator';

interface Props {
  io: IComentatorIo;
  roomName: string;
  rooms: IRooms;
  seconds: number;
  roomsWithGame: IRoomsWithGame;
}

export const checkStatusEndGame = ({ io, roomName, rooms, seconds, roomsWithGame }: Props) => {
  const room = rooms.getRoom(roomName);
  if (!room) {
    return;
  }
  const users = Array.from(room.users);
  const usersWithEndGame = users.filter(([_, value]) => value.endGame).length;
  if (Boolean(seconds) && usersWithEndGame !== users.length) {
    io.to(roomName).emit(SOCKET_EVENT.changTimerForGame, seconds);
    return;
  }

  roomsWithGame.removeRoom(roomName);

  const leaders = setLeaders(users, room.leaders);

  io.to(roomName).emit(SOCKET_EVENT.endedGame, [...room.leaders, ...leaders]);

  rooms.endedGame(roomName);
  rooms.resetRoom(roomName);

  io.to(roomName).emit(SOCKET_EVENT.updateRoom, rooms.mapUsers(roomName));
  io.sockets.emit(SOCKET_EVENT.updateRooms, rooms.mapRooms());
};
