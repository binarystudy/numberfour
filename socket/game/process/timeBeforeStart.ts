import { Server } from 'socket.io';
import { SOCKET_EVENT } from '../../config';
import { IComentatorIo } from '../../comentator';

interface PropsBeforeEnd {
  io: IComentatorIo;
  roomName: string;
}

interface PropsBeforeStart extends PropsBeforeEnd {
  seconds: number;
}

export const timeBeforeStart = ({ io, roomName, seconds }: PropsBeforeStart) => {
  io.to(roomName).emit(SOCKET_EVENT.changTimerBeforeStart, seconds);
};

export const timeBeforeEnd = ({ io, roomName }: PropsBeforeEnd) => {
  io.to(roomName).emit(SOCKET_EVENT.endTimerBeforeGame);
};
