import { SECONDS_FOR_GAME } from '../../config';
import { IRooms, IUser } from '../../data';
import { lineLeadersName } from '../../helpers';

interface Props {
  roomName: string;
  rooms: IRooms;
  seconds: number;
}

export const checkStatusProgress = ({ roomName, rooms, seconds }: Props) => {
  const room = rooms.getRoom(roomName);
  if (!room) {
    return;
  }

  const roomLeaders = lineLeadersName(room.leaders);
  const leaders = Array.from(room.users)
    .filter((user) => !roomLeaders.includes(user[0]))
    .filter((user) => user[1].endGame)
    .map(([name, _]) => [name, SECONDS_FOR_GAME - seconds]) as [string, number][];

  if (!leaders.length) {
    return;
  }

  rooms.setRoom(roomName, {
    ...room,
    leaders: [...room.leaders, leaders],
  });
};
