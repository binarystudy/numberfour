import { checkStatusEndGame } from './checkStatusEndGame';
import { timeBeforeStart, timeBeforeEnd } from './timeBeforeStart';
import { checkStatusProgress } from './checkStatusProgress';
import { rooms, roomsWithGame } from '../../data';
import { IComentatorIo } from '../../comentator';

export const processGame = (io: IComentatorIo) => {
  roomsWithGame.getBefore().forEach(([roomName, value]) => {
    timeBeforeStart({ io, roomName, seconds: value.secondsBeforeStart });
  });
  roomsWithGame.getNewGame().forEach(([roomName, _]) => {
    timeBeforeEnd({ io, roomName });
  });
  roomsWithGame.getGame().forEach(([roomName, value]) => {
    checkStatusProgress({ roomName, rooms, seconds: value.secondsForGame });
    checkStatusEndGame({ io, rooms, roomName, seconds: value.secondsForGame, roomsWithGame });
  });
  roomsWithGame.changeTime();
};
