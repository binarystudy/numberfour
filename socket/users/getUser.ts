import { Socket } from 'socket.io';
import { users } from '../data';

export const getUser = (socket: Socket): string | undefined => {
  const { username } = socket.handshake.query;
  if (!username || Array.isArray(username) || users.has(username)) {
    return;
  }
  users.set(username, 0);
  return username;
};
