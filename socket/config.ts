export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 3;
export const SECONDS_FOR_GAME = 60;
export const SECONDS_FOR_COMMENTATOR = 48;
export const GAME_INTERVAL = 1000;
export const MAX_LENGTH_LEADERS = 3;
export const MIN_LETTERS_FOR_FINISH_COMMENT = 30;

export const SOCKET_EVENT = {
  updateRooms: 'UPDATE_ROOMS',
  updateRoom: 'UPDATE_ROOM',
  leaveRoom: 'LEAVE_ROOM',
  leavedRoom: 'LEAVED_ROOM',
  joinRoomDone: 'JOIN_ROOM_DONE',
  joinRoom: 'JOIN_ROOM',
  createRoom: 'CREATE_ROOM',
  createdRoom: 'CREATED_ROOM',
  changeStatus: 'CHANGE_STATUS',
  startGame: 'START_GAME',
  updateProgress: 'UPDATE_PROGRESS',
  updatedProgress: 'UPDATED_PROGRESS',
  endGame: 'END_GAME',
  endedGame: 'ENDED_GAME',
  connectionUser: 'CONNECTION_USER',
  changTimerBeforeStart: 'CHANGE_TIMER_BEFORE_START',
  changTimerForGame: 'CHANGE_TIMER_FOR_GAME',
  disconnecting: 'disconnecting',
  endTimerBeforeGame: 'END_TIMER_BEFORE_GAME',
  comment: 'COMMENT',
};
