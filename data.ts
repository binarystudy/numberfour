const texts = [
  `Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis odio perferendis tempore nesciunt quibusdam consequuntur quam eligendi omnis enim optio? Suscipit itaque autem error veritatis assumenda recusandae. Mollitia, fuga obcaecati.`,
  'Lorem ipsum dolor sit amet conse',
  'Text for typing #3',
  'Text for typing #4',
  'Text for typing #5',
  'Text for typing #6',
  'Text for typing #7',
];

const message = {
  createRoomGreeting: 'Доброго дня! Ви створили кімнату, я ваш сьогодняшній коментатор!',
  joinRoomDoneGreeting: 'Доброго дня! Ви приеднались до кімнати, я ваш сьогодняшній коментатор!',
};

export { texts, message };
