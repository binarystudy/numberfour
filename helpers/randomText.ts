import { texts } from '../data';

const random = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const randomText = () => {
  return random(0, texts.length - 1);
};
