import { createElement } from '../../helpers/index.mjs';

export function showModal({ title, bodyElement, onClose = () => {} }) {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }) {
  const header = createHeader(title, onClose);

  const modalContainer = createElement({
    tagName: 'div',
    attributes: { class: 'modal-root' },
    child: [
      header,
      createElement({ tagName: 'div', attributes: { class: 'modal-body' }, child: bodyElement }),
    ],
  });

  return createElement({
    tagName: 'div',
    attributes: { class: 'modal-layer' },
    child: modalContainer,
  });
}

function createHeader(title, onClose) {
  const titleElement = createElement({ tagName: 'span', child: title });
  const closeButton = createElement({
    tagName: 'div',
    attributes: {
      class: 'close-btn',
      id: 'quit-results-btn',
    },
    child: '×',
  });

  const close = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);

  return createElement({
    tagName: 'div',
    attributes: { class: 'modal-header' },
    child: [titleElement, closeButton],
  });
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
