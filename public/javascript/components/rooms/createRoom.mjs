import { createElement } from '../../helpers/index.mjs';
import { ELEMENT } from '../../config/index.mjs';

export const createRoom = ([roomName, countUsers]) => {
  const users = createElement({
    tagName: 'span',
    child: `${countUsers} ${countUsers > 1 ? 'users' : 'user'} in room`,
  });

  const title = createElement({ tagName: 'h3', child: roomName });

  const button = createElement({
    tagName: 'button',
    attributes: {
      class: 'button join-btn btn-primary btn-sm',
      [`data-${ELEMENT.dataSetNameRoom}`]: roomName,
    },
    child: 'Join',
  });

  return createElement({
    tagName: 'div',
    attributes: {
      class: 'room',
    },
    child: [users, title, button],
  });
};
