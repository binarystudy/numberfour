import { ELEMENT, CLASSES } from '../config/index.mjs';
import { removeClass, addClass, createElement } from '../helpers/index.mjs';
import { createLeftField } from './game/createLeftField.mjs';
import { createRigthField } from './game/createRigthField.mjs';

const root = document.getElementById(ELEMENT.gamePage);

export const gamePage = (props) => {
  if (!root) {
    return;
  }
  const leftField = createLeftField(props.users, props.roomName);

  const rigthField = createRigthField(props.users);

  root.innerHTML = '';
  root.append(
    createElement({
      tagName: 'div',
      attributes: {
        class: 'wrapper game-fields',
      },
      child: [leftField, rigthField],
    })
  );
};

export const gamePageDisplayNone = () => {
  if (!root) {
    return;
  }
  addClass(root, CLASSES.displayNone);
};

export const gamePageDisplay = () => {
  if (!root) {
    return;
  }
  removeClass(root, CLASSES.displayNone);
};
