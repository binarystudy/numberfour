import { createElement } from '../helpers/index.mjs';
import { ELEMENT, CLASSES } from '../config/index.mjs';
import { createRoom } from './rooms/createRoom.mjs';
import { removeClass, addClass } from '../helpers/index.mjs';

const root = document.getElementById(ELEMENT.pageRooms);
let rootRooms = null;

export const roomsPage = (rooms) => {
  if (!roomsPage) {
    return;
  }
  const roomsElements = rooms.map((room) => createRoom(room));
  if (rootRooms) {
    rootRooms.innerHTML = '';
    rootRooms.append(...roomsElements);
    return;
  }
  rootRooms = createElement({
    tagName: 'div',
    attributes: {
      class: 'rooms',
      id: 'rooms',
    },
    child: roomsElements,
  });
  const title = createElement({
    tagName: 'h2',
    attributes: {
      class: 'title',
    },
    child: 'Join Room Or Create New',
  });
  const button = createElement({
    tagName: 'button',
    attributes: {
      id: 'add-room-btn',
      class: 'button btn-primary',
    },
    child: 'Create Room',
  });
  root.append(
    createElement({
      tagName: 'div',
      attributes: {
        class: 'wrapper',
      },
      child: [title, button, rootRooms],
    })
  );
};

export const roomsPageDisplayNone = () => {
  if (!root) {
    return;
  }
  addClass(root, CLASSES.displayNone);
};

export const roomsPageDisplay = () => {
  if (!root) {
    return;
  }
  removeClass(root, CLASSES.displayNone);
};
