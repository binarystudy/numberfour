import { createElement } from '../../helpers/index.mjs';
import { ELEMENT } from '../../config/index.mjs';

let gameField = null;
let messageBox = null;

export const commentatorBox = (message) => {
  if (!gameField || !messageBox) {
    gameField = document.getElementById(ELEMENT.gameField);
    messageBox = createElement({
      tagName: 'span',
      attributes: {
        class: 'commentator-message',
      },
    });
    gameField.append(messageBox);
  }
  messageBox.innerHTML = message;
};

export const commentatorBoxHide = () => {
  if (messageBox) {
    messageBox.remove();
    messageBox = null;
  }
};
