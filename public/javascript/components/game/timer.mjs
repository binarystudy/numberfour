import { ELEMENT } from '../../config/index.mjs';
import { createElement, timer } from '../../helpers/index.mjs';

let gameField = null;
let timerElement = null;

export const timerBeforeGame = (seconds) => {
  if (!gameField || !timerElement) {
    gameField = document.getElementById(ELEMENT.gameField);
    timerElement = createElement({
      tagName: 'div',
      attributes: {
        id: ELEMENT.timer,
      },
    });
    gameField.append(timerElement);
  }
  timerElement.innerHTML = seconds;
};

export const timerGameHide = () => {
  timerElement.remove();
  timerElement = null;
};

export const timerGame = (seconds) => {
  if (!timerElement) {
    timerElement = createElement({
      tagName: 'span',
      attributes: {
        class: 'wrapper-timer-game',
      },
    });
    gameField.append(timerElement);
  }
  timerElement.innerHTML = `${seconds} seconds left`;
};
