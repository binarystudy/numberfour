import { ELEMENT, CLASSES } from '../../config/index.mjs';
import { createElement, addClass, removeClass, storageUserName } from '../../helpers/index.mjs';

let button = null;

const createText = (users) => {
  const user = users.filter(([name, _]) => storageUserName.get() === name);
  return `${user.length && user[0][1].ready ? 'Not Ready' : 'Ready'}`;
};

export const createReadyButton = (users) => {
  button = createElement({
    tagName: 'button',
    attributes: {
      id: ELEMENT.buttonReadyId,
      class: 'button btn-primary btn-large',
    },
    child: createText(users),
  });
  return button;
};

export const changeReadyButton = (users) => {
  if (!button) {
    return;
  }
  button.innerHTML = createText(users);
};

export const readyButtonHide = () => {
  if (!button) {
    return;
  }
  addClass(button, CLASSES.displayNone);
};

export const readyButtonShow = () => {
  if (!button) {
    return;
  }
  removeClass(button, CLASSES.displayNone);
};
