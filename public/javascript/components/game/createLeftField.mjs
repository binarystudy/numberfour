import { createListUsers } from './createListUsers.mjs';
import { createButtonBackToRooms } from './backToRooms.mjs';
import { createElement } from '../../helpers/index.mjs';

export const createLeftField = (users, roomName) => {
  const listUsers = createListUsers(users);
  const title = createElement({
    tagName: 'h2',
    attributes: {
      class: 'title',
    },
    child: roomName,
  });
  const back = createButtonBackToRooms();
  return createElement({
    tagName: 'div',
    child: [title, back, listUsers],
  });
};
