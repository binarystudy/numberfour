import { createReadyButton } from './readyButton.mjs';
import { createElement } from '../../helpers/index.mjs';
import { ELEMENT } from '../../config/index.mjs';

export const createRigthField = (users) => {
  const readyButton = createReadyButton(users);

  return createElement({
    tagName: 'div',
    attributes: {
      class: 'game-field-right',
      id: ELEMENT.gameField,
    },
    child: readyButton,
  });
};
