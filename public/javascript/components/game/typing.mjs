import { ELEMENT } from '../../config/index.mjs';
import { createElement } from '../../helpers/index.mjs';
import { updateGame } from '../../sockets/index.mjs';

let typingElement = null;

export const removeTypingElement = () => {
  typingElement.remove();
};

export const typing = (text) => {
  typingElement = createElement({
    tagName: 'div',
    attributes: {
      id: ELEMENT.gameText,
    },
    child: text,
  });
  const gameField = document.getElementById(ELEMENT.gameField);
  gameField.append(typingElement);
  let index = 0;

  return (code) => {
    if (text[index] !== code || index === text.length) {
      return;
    }

    index++;

    updateGame(index);

    const typed = text.slice(0, index);
    const endOfText = text.slice(index);

    const highlighter = createElement({
      tagName: 'span',
      attributes: { class: 'highlight' },
      child: typed,
    });
    const underline = createElement({
      tagName: 'span',
      attributes: {
        class: 'underline',
      },
      child: endOfText.slice(0, 1),
    });
    const other = createElement({
      tagName: 'span',
      child: endOfText.slice(1),
    });
    typingElement.innerHTML = '';
    typingElement.append(highlighter, underline, other);
  };
};
