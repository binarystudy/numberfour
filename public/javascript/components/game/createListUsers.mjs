import { createElement, storageUserName } from '../../helpers/index.mjs';
import { ELEMENT } from '../../config/index.mjs';

let root = null;

export const createUser = (name, ready, progress) => {
  const username = storageUserName.get();
  const indicator = createElement({
    tagName: 'span',
    attributes: {
      class: `ready-status${ready ? ' ready-status-green' : ' ready-status-red'}`,
    },
  });
  const title = createElement({
    tagName: 'div',
    attributes: {
      class: 'name',
    },
    child: `${name}${username === name ? ' (you)' : ''}`,
  });

  const progressElement = createElement({
    tagName: 'div',
    attributes: {
      class: 'wrapper-progress',
    },
    child: createElement({
      tagName: 'div',
      attributes: {
        class: `${ELEMENT.userProgress} ${name}${progress === 100 ? ' finished' : ''}`,
        style: `width: ${progress}%`,
      },
    }),
  });

  return createElement({
    tagName: 'div',
    attributes: {
      class: 'user',
    },
    child: [indicator, title, progressElement],
  });
};

const createRoot = () => {
  root = createElement({
    tagName: 'div',
    attributes: {
      class: 'users',
      id: ELEMENT.listUsers,
    },
  });
};

const createList = (users) =>
  users.map(([name, { ready, progress }]) => createUser(name, ready, progress));

export const createListUsers = (users) => {
  root = createElement({
    tagName: 'div',
    attributes: {
      class: 'users',
      id: ELEMENT.listUsers,
    },
    child: [...createList(users)],
  });
  return root;
};

export const changeListUsers = (props) => {
  if (!root) {
    return;
  }
  root.innerHTML = '';
  root.append(...createList(props.users));
};
