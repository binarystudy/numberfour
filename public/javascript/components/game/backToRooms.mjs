import { createElement, addClass, removeClass } from '../../helpers/index.mjs';
import { ELEMENT, CLASSES } from '../../config/index.mjs';

let button = null;

export const createButtonBackToRooms = () => {
  button = createElement({
    tagName: 'button',
    attributes: {
      id: ELEMENT.buttonBackId,
      class: 'button btn-primary',
    },
    child: 'Back to Rooms',
  });
  return button;
};

export const buttonBackToRoomsHide = () => {
  if (!button) {
    return;
  }
  addClass(button, CLASSES.displayNone);
};

export const buttonBackToRoomsShow = () => {
  if (!button) {
    return;
  }
  removeClass(button, CLASSES.displayNone);
};
