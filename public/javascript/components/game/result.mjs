import { createElement } from '../../helpers/index.mjs';
import { showModal } from '../helpers/modal.mjs';
import { buttonBackToRoomsShow } from './backToRooms.mjs';
import { readyButtonShow } from './readyButton.mjs';
import { removeTypingElement } from './typing.mjs';

const onClose = () => {
  buttonBackToRoomsShow();
  readyButtonShow();
  removeTypingElement();
};

export const resultGame = (results) => {
  const list = results.map((userNames, index) =>
    createElement({
      tagName: 'div',
      attributes: {
        id: `place-${index + 1}`,
      },
      child: `${index + 1}: ${userNames.join(', ')}`,
    })
  );
  const body = createElement({
    tagName: 'div',
    attributes: {
      class: 'wrapper-results',
    },
    child: [...list],
  });
  showModal({ title: 'Game Results!', bodyElement: body, onClose });
};
