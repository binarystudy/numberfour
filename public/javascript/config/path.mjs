export const PATH = {
  login: '/login',
  game: '/game',
  gameTexts: '/game/texts/',
};
