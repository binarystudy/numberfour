export const ELEMENT = {
  pageRooms: 'rooms-page',
  gamePage: 'game-page',
  createButtonId: 'add-room-btn',
  buttonReadyId: 'ready-btn',
  buttonBackId: 'quit-room-btn',
  dataSetNameRoom: 'nameroom',
  listUsers: 'users',
  gameField: 'game-field',
  gameText: 'text-container',
  timer: 'timer',
  userProgress: 'user-progress',
};

export const CLASSES = {
  displayNone: 'display-none',
};
