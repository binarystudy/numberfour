import {
  roomsPage,
  roomsPageDisplay,
  gamePageDisplayNone,
  changeListUsers,
  changeReadyButton,
} from '../components/index.mjs';
import { storageUserName } from '../helpers/index.mjs';
import { socket } from '../index.mjs';

export const updateRoom = (response) => {
  if (!response.beginGame) {
    changeReadyButton(response.users);
  }
  changeListUsers(response);
};

export const leaveRoom = () => {
  socket.emit('LEAVE_ROOM', storageUserName.get());
};

export const leavedRoom = (response) => {
  if (response === 'error') {
    return;
  }
  roomsPageDisplay();
  roomsPage(response);
  gamePageDisplayNone();
};

export const changeStatus = () => {
  socket.emit('CHANGE_STATUS', storageUserName.get());
};
