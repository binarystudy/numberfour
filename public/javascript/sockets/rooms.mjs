import {
  roomsPageDisplayNone,
  gamePageDisplay,
  gamePage,
} from '../components/index.mjs';
import { storageUserName } from '../helpers/index.mjs';
import { socket } from '../index.mjs';

export const createRoomEmit = (roomName) => {
  socket.emit('CREATE_ROOM', { roomName, userName: storageUserName.get() });
};

export const createdRoom = (response) => {
  if (response === 'error') {
    alert(`Such room already exist.`);
    return;
  }
  roomsPageDisplayNone();
  gamePage(response);
  gamePageDisplay();
};

export const joinRoomEmit = (roomName) => {
  socket.emit('JOIN_ROOM', { roomName, userName: storageUserName.get() });
};

export const joinRoomDone = ({ roomName, users }) => {
  roomsPageDisplayNone();
  gamePage({ roomName, users });
  gamePageDisplay();
};

