import {
  buttonBackToRoomsHide,
  buttonBackToRoomsShow,
  readyButtonHide,
  readyButtonShow,
  timerBeforeGame,
  timerGame,
  typing,
  removeTypingElement,
  timerGameHide,
} from '../components/index.mjs';
import { socket } from '../index.mjs';
import { PATH } from '../config/index.mjs';
import { storageUserName } from '../helpers/index.mjs';
import { addEventKeyDown, removeEventKeyDown } from '../events/index.mjs';

let text = '';

const getText = async (idText) => {
  try {
    const responseGet = await fetch(`${PATH.gameTexts}${idText}`);
    text = await responseGet.json();
  } catch (e) {
    console.error(e?.message || 'unknown error');
  }
};

export const gameOn = async ({ idText }) => {
  if (idText === undefined) {
    return;
  }
  await getText(idText);
  buttonBackToRoomsHide();
  readyButtonHide();
};

export const updateGame = (progress) => {
  socket.emit('UPDATE_PROGRESS', {
    userName: storageUserName.get(),
    numberLittersString: progress,
  });
};

export const changeTimerBeforeGame = (secondsBeforeStart) => {
  timerBeforeGame(secondsBeforeStart);
};

export const endTimerBeforeGame = () => {
  timerGameHide();
  addEventKeyDown(typing(text));
};

export const changeTimerForGame = (secondsForGame) => {
  timerGame(secondsForGame);
};

export const gameOff = (response) => {
  removeEventKeyDown();
  timerGameHide();
  buttonBackToRoomsShow();
  readyButtonShow();
  removeTypingElement();
};
