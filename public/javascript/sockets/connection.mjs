import { PATH } from '../config/index.mjs';
import { storageUserName } from '../helpers/index.mjs';

export const connectionUser = (message) => {
  if (message === 'error') {
    storageUserName.remove();
    alert(`Such user already exist.`);
    window.location.replace(PATH.login);
  }
};
