export const addClass = (element, classes) => {
  element.classList.add(...formatClasses(classes));
};

export const removeClass = (element, classes) => {
  element.classList.remove(...formatClasses(classes));
};

const formatClasses = (classes) => classes.split(' ').filter(Boolean);
