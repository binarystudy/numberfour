export const createElement = (props) => {
  const parent = document.createElement(props.tagName);
  if (props.attributes) {
    Object.entries(props.attributes).forEach(([key, value]) => {
      parent.setAttribute(key, value);
    });
  }
  if (!props.child) {
    return parent;
  }
  if (Array.isArray(props.child)) {
    props.child.filter((el) => el).map((el) => parent.append(el));
  } else {
    parent.append(props.child);
  }
  return parent;
};
