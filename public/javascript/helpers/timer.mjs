export const timer = async (cb, interval = 1000, stopTimer) => {
  return await new Promise((resolve) => {
    setTimeout(function bTimer() {
      cb && cb();
      if (stopTimer && stopTimer()) {
        resolve();
      } else {
        setTimeout(bTimer, interval);
      }
    }, interval);
  });
};
