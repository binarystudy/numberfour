import { STORAGE_NAME } from '../config/index.mjs';

class Storage {
  constructor(name) {
    this.storageName = name;
  }

  get() {
    return sessionStorage.getItem(this.storageName);
  }

  set(value) {
    sessionStorage.setItem(this.storageName, value);
  }

  remove() {
    sessionStorage.removeItem(this.storageName);
  }
}

export const storageUserName = new Storage(STORAGE_NAME.userName);
