import { storageUserName } from './helpers/storage.mjs';
import { PATH } from './config/index.mjs';
import { roomsPage } from './components/index.mjs';
import { addEventClick } from './events/index.mjs';
import {
  connectionUser,
  createdRoom,
  joinRoomDone,
  leavedRoom,
  updateRoom,
  gameOn,
  gameOff,
  changeTimerBeforeGame,
  changeTimerForGame,
  endTimerBeforeGame,
  comentatorGreeting,
} from './sockets/index.mjs';

const username = storageUserName.get();

if (!username) {
  window.location.replace(PATH.login);
}

addEventClick();

export const socket = io('', { query: { username } });

socket.on('CONNECTION_USER', connectionUser);

socket.on('CREATED_ROOM', createdRoom);

socket.on('JOIN_ROOM_DONE', joinRoomDone);

socket.on('UPDATE_ROOMS', roomsPage);

socket.on('UPDATE_ROOM', updateRoom);

socket.on('UPDATED_PROGRESS', updateRoom);

socket.on('LEAVED_ROOM', leavedRoom);

socket.on('START_GAME', gameOn);

socket.on('ENDED_GAME', gameOff);

socket.on('CHANGE_TIMER_BEFORE_START', changeTimerBeforeGame);

socket.on('END_TIMER_BEFORE_GAME', endTimerBeforeGame);

socket.on('CHANGE_TIMER_FOR_GAME', changeTimerForGame);

socket.on('COMMENT', comentatorGreeting);
