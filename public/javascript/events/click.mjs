import { createRoomEmit, joinRoomEmit, leaveRoom, changeStatus } from '../sockets/index.mjs';
import { ELEMENT } from '../config/index.mjs';

export const addEventClick = () => {
  document.addEventListener('click', handleClick);
};

const createRoom = () => {
  let name;
  while (true) {
    name = prompt('Enter name room!');
    if ((name && !!name.length) || name === null) {
      break;
    }
  }
  if (name) {
    createRoomEmit(name);
  }
};

function handleClick(event) {
  const id = event.target.id;
  const nameRoom = event.target.dataset[ELEMENT.dataSetNameRoom];
  switch (id) {
    case ELEMENT.createButtonId:
      createRoom();
      return;
    case ELEMENT.buttonBackId:
      leaveRoom();
      return;
    case ELEMENT.buttonReadyId:
      changeStatus();
      return;
  }
  nameRoom && joinRoomEmit(nameRoom);
}
