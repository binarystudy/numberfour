let typingWithText;

const handleKeyDown = (event) => {
  typingWithText(event.key);
};

export const addEventKeyDown = (cb) => {
  typingWithText = cb;
  document.addEventListener('keydown', handleKeyDown);
};

export const removeEventKeyDown = () => {
  document.removeEventListener('keydown', handleKeyDown);
};
