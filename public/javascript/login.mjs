import { storageUserName } from './helpers/storage.mjs';
import { PATH } from './config/index.mjs';

const username = storageUserName.get();

if (username) {
  window.location.replace(PATH.game);
}

const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  storageUserName.set(inputValue);
  window.location.replace(PATH.game);
};

const onKeyUp = (ev) => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
